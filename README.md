# wifi-web

Web interface for the wifi management

### Setup environment

**get the application**

```sh
git clone https://gitlab.com/ragulkanth/wifi-web.git
```
then get into the appliction directory

**create the virtualenv**
```sh
virtualenv --python=python3 .venv
```
**activating the environment**
```sh
source .venv/bin/activate
```
**installing the dependencies**
```sh
pip3 install -r requirements.txt
```

**make sure that _.env_ file is loaded**
```sh
source .env
```

**running the application**
```sh
python3 app.py
```