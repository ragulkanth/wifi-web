from app import app
from flask import url_for

WIFI_ON = "nmcli radio wifi on" 
WIFI_OFF = "nmcli radio wifi off"
WIFI_STATUS = "nmcli radio wifi"
WIFI_LIST = "nmcli -g IN-USE,SSID,SECURITY,SIGNAL dev wifi list"
WIFI_SCAN = "nmcli dev wifi rescan"
WIFI_CONNECT = ("nmcli", "-w", "0", "dev", "wifi", "connect")
WIFI_CONNECT_EXIST = ("nmcli", "-w", "0", "connection", "up")
WIFI_CONNECTION_LIST = ("nmcli", "-t", "-g", "name", "connection", "show")


def shell_exec(cmd):
    if type(cmd) is not list:
        cmd = cmd.split(' ') if type(cmd) is str else cmd
    import subprocess
    return subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="UTF-8")

def wifi_status():
    check = shell_exec(WIFI_STATUS)
    if check.returncode == 0 and check.stdout == "enabled\n":
        return True
    return False

def wifi_on_off():
    cmd = WIFI_OFF if wifi_status() else WIFI_ON
    cmd_exec = shell_exec(cmd)
    if cmd_exec.returncode > 0:
        return False
    return True

def wifi_list():
    wifi_lst = []
    cmd_exec = shell_exec(WIFI_LIST)
    if cmd_exec.returncode == 0 and cmd_exec.stdout:
        wifi_lst = [tuple(i.split(":")) for i in cmd_exec.stdout[:-1].split("\n")]
    return wifi_lst
    
def wifi_connected_to():
    wifi_lst = wifi_list()
    for wifi in wifi_lst:
        if wifi[0] == '*':
            return wifi
    return ()

def wifi_scan():
    scan = shell_exec(WIFI_SCAN)
    if scan.returncode > 0:
        return scan.stderr[:-1]
    return True

def wifi_connect(ssid, passphrase=None, hidden=False):
    if ssid in wifi_connection_list():
        cmd = list(WIFI_CONNECT_EXIST)
        cmd.append(ssid)
        return shell_exec(cmd)
    cmd = list(WIFI_CONNECT)
    cmd.append(ssid)
    if passphrase:
        cmd.append('password')
        cmd.append(passphrase)
    if hidden:
        cmd.append('hidden')
        cmd.append('yes')
    return shell_exec(cmd)

def wifi_connection_list():
    cmd_exec = shell_exec(list(WIFI_CONNECTION_LIST))
    return cmd_exec.stdout[:-1].split("\n")



@app.context_processor
def utility_processor():
    def status_bg(power, connected_to):
        if power:
            if connected_to:
                return "#ccff90"
            return "#faff81"
        return "#ffccbc"
    def signal_strength(strength):
        strength = int(strength)
        strength_symbol = "hundred.svg"
        if strength > 75:
            strength_symbol = "hundred.svg"
        elif strength > 50:
            strength_symbol = "seventyfive.svg"
        elif strength > 40:
            strength_symbol = "fifty.svg"
        elif strength > 20:
            strength_symbol = "twentyfive.svg"
        else:
            strength_symbol = "zero.svg"
        return url_for('static', filename='img/' + strength_symbol)
    return dict(status_bg=status_bg, signal_strength=signal_strength)
