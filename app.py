from os import getenv 

from flask import Flask, session
from flask_session import Session

app = Flask(__name__)

#csrf config
#app.config['CSRF_SECRET_KEY'] = b'rf5yQkNh00W54EgLBttbnsw8iiZmkk75nDPV'

#session initiated
SESSION_TYPE = 'filesystem'
SESSION_FILE_DIR = '/tmp'
SESSION_FILE_THRESHOLD = 500
#SESSION_FILE_MODE = "0600"

app.config.from_object(__name__)
Session(app)

#importing the views
from views import *

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="5000")
