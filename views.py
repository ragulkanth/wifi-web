from datetime import datetime
from flask import request, render_template, flash, send_file, redirect, url_for

from helpers import *

@app.route('/', methods=['GET', 'POST'])
def index():
    status = {
            'power': wifi_status(),
            'connection': wifi_connected_to()
            }
    networks = wifi_list()
    if request.method == "POST":
        ssid = request.form.get('ssid', '')
        passphrase = request.form.get('passphrase', '')
        hidden = True if request.form.get('hidden', '') else False
        stat = wifi_connect(ssid, passphrase, hidden)
        print(stat.returncode)
        print(stat.stdout)
        print(stat.stderr)
        return redirect(url_for('index'))
    if request.args.get('cssid', ''):
        stat = wifi_connect(request.args.get('cssid', ''))
        print(stat.returncode)
        print(stat.stdout)
        print(stat.stderr)
        return redirect(url_for('index'))

    return render_template('index.html', status=status, networks=networks)

@app.route('/on_off', methods=['GET'])
def on_off():
    wifi_on_off()
    return redirect(url_for('index'))

@app.route('/rescan', methods=['GET'])
def rescan():
    wifi_scan()
    if wifi_status():
        shell_exec("sleep 15")
    return redirect(url_for('index'))
